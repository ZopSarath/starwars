import React, {useState} from 'react'
import { useQuery } from 'react-query'
import Person from './Person'

const fetchPeople = async (page) => {
    const res = await fetch(`http://swapi.dev/api/people/?page=${page}`)
  return res.json()
}

const People = () => {
    const [ pageNo, setPageNo ] = useState(1)
    const { data, status } = useQuery('people',()=>fetchPeople(pageNo), {keepPreviousData: true})

  return (
    <div>
      <h2>People</h2>
      {/* { status } */}

      {status === 'loading' && (
        <div>Loading data.......</div>
      )}

      {status === 'error' && (
        <div>Error fetching data</div>
      )}

      {status === 'success' && (
        <> 
        <button onClick={()=> setPageNo(old => Math.max(old-1,1))} disabled={pageNo === 1}>Previous page</button>
        <span>{pageNo}</span>
        <button onClick={()=> setPageNo(old => (!data || !data.next ? old : old + 1))} disabled={!data || !data.next}>Next page</button>
        <div>
          { data.results.map(person => <Person key={person.name} person={person} /> ) }
            </div>
        </>
       
      )} 
    </div>
  );
}
 
export default People