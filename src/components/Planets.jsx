import React, {useState} from 'react'
import {useQuery} from 'react-query'
import Planet from './Planet'

const fetchPlanets = async (page) => {
    const res = await fetch(`http://swapi.dev/api/planets/?page=${page}`)
    return res.json()
}

const Planets = () => {
    const [ pageNo, setPageNo ] = useState(1)
    const { data, status } = useQuery('planets',()=>fetchPlanets(pageNo));
    console.log(data)
    return (
    <div>
        <h2>Planets</h2>
        {/* <p>{status}</p> */}

        <button onClick={()=> setPageNo(1)}>Page 1</button>
        <button onClick={()=> setPageNo(2)}>Page 2</button>
        <button onClick={()=> setPageNo(3)}>Page 3</button>
        {status === 'loading' && (
        <div>Loading data.....</div>
        )}

        {status === 'error' && (
        <div>Error Fetching data</div>
        )}

        {status === 'success' && (
        <div>
            {data.results.map(planet => <Planet key={planet.name} planet={planet}/>)}
        </div>
        )}
    </div>
        
    )
}

export default Planets
